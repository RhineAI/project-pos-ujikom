<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController; 
use App\Http\Controllers\DashboardController; 

/* PRODUCT */
use App\Http\Controllers\CategoryController; 
use App\Http\Controllers\UnitController; 
use App\Http\Controllers\ProductController; 

/*  PEOPLE */
use App\Http\Controllers\MemberController; 
use App\Http\Controllers\UserController; 

/*  SUPPLIER */
use App\Http\Controllers\SupplierController; 

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::post('/login', [LoginController::class, 'authenticate'])->name('login.authenticate');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

Route::middleware(['auth'])->group(function () {

    Route::get('/', [DashboardController::class, 'index'])->name('dashboard.index');

    //Category
    Route::resource('/categories', CategoryController::class);
    Route::post('/category/data', [CategoryController::class, 'data'])->name('category.data');

    // Unit
    // Route::resource('/units', UnitController::class);
    // Route::post('/unit/data', [UnitController::class, 'data'])->name('unit.data');

    // Product
    Route::resource('/products', ProductController::class);
    Route::post('/product/data', [ProductController::class, 'data'])->name('product.data');

    // Member
    Route::resource('/members', MemberController::class);
    Route::post('/member/data', [MemberController::class, 'data'])->name('member.data');

    // Supplier
    Route::resource('/suppliers', SupplierController::class);
    Route::post('/supplier/data', [SupplierController::class, 'data'])->name('supplier.data');

    // User
    Route::resource('/users', UserController::class);
    Route::post('/user/data', [UserController::class, 'data'])->name('user.data');

});