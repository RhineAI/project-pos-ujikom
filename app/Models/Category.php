<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table = 'category';
    protected $primaryKey = 'id';
    protected $guarded = [];
    // protected $fillable = 'updated_at';

    // public $timestamps = true;

    public function product()
    {
        return $this->hasMany(Product::class);
    }
}
