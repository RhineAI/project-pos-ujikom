<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Unit;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unit['unit'] = Unit::get();
        return view('product.unit.index')->with($unit);
        // return view('product.unit.index');
    }


    public function data() 
    {
        $unit = Unit::orderBy('id', 'DESC')->get();

        return datatables()
            ->of($unit)
            ->addIndexColumn()
            ->addColumn('created_at', function ($unit) {
                return $unit->created_at->format('d M Y');
            })
            ->addColumn('last_updated', function ($unit) {
                return $unit->updated_at->diffForHumans();
            })
            ->addColumn('aksi', function ($unit) {
                return '
                    <button data-unit="'.$unit->unit_name.'" data-route="'. route('units.update', $unit->id) .'" class="edit btn btn-xs btn-success btn-flat"><i class="bi bi-pencil-square"></i></button>
                    <button onclick="deleteData(`'. route('units.destroy', $unit->id) .'`)" class="btn btn-xs btn-danger btn-flat delete"><i class="bi bi-trash"></i></button>
                ';
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'unit_name' => 'required|max:225|unique:unit',
        ]);

        $name = $request['unit_name'];

        $unit = new Unit;
        $unit->unit_name = $name;
        $unit->save();

        return redirect()->route('units.index')->with(['success' => 'Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $request->validate([
            'unit_name' => 'max:225|unique:unit',
        ]);

        $name = $request['unit_name'];

        $unit = Unit::find($id);
        $unit->unit_name = $name;
        $unit->update();

        return redirect()->route('units.index')->with(['success' => 'Berhasil di Update!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Unit::find($id)->delete();
        return redirect()->route('units.index')->with(['success' => 'Berhasil Dihapus!']);
    }
}
