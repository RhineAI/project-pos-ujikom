<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class DashboardController extends Controller
{
    public function index() {
        $product['product'] = Product::count();
        // return view('product.unit.index')->with($unit);
        return view('dashboard.index')->with($product);
    }
}
