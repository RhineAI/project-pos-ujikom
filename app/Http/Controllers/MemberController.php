<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('member.index');
    }

    public function data() 
    {
        $member = Member::orderBy('id', 'DESC')->get();

        return datatables()
            ->of($member)
            ->addIndexColumn()
            ->addColumn('grade', function($member) {
                if ($member->level == 1) {
                    return '<span class="badge badge-dark"> Basic </span>';
                }
                elseif ($member->level == 2) {
                    return '<span class="badge badge-success"> VIP </span>';
                }
                elseif ($member->level == 3) {
                    return '<span class="badge badge-info luxury"><i> Luxury </i></span>';

                }
            }) 
            ->addColumn('created_at', function ($member) {
                return $member->created_at->format('d M Y');
            })
            ->addColumn('last_updated', function ($member) {
                return $member->updated_at->diffForHumans();
            })
            ->addColumn('aksi', function ($member) {
                return '
                    <button data-name="'.$member->name.'" data-adress="'.$member->adress.'" data-level="'.$member->level.'" data-route="'. route('members.update', $member->id) .'" class="edit btn btn-xs btn-success btn-flat"><i class="bi bi-pencil-square"></i></button>
                    <button onclick="deleteData(`'. route('members.destroy', $member->id) .'`)" class="btn btn-xs btn-danger btn-flat delete"><i class="bi bi-trash"></i></button>
                ';
            })
            ->rawColumns(['aksi', 'grade'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $member = new Member;
        $member->name = $request->name;
        $member->level = $request->level;
        $member->adress = $request->adress;
        $member->save();

        return redirect()->route('members.index')->with(['success' => 'Berhasil Disimpan!']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $member = Member::find($id);
        $member->name = $request->name;
        $member->adress = $request->adress;
        $member->level = $request->level;
        $member->update();

        return redirect()->route('members.index')->with(['success' => 'Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Member::find($id)->destroy();
        return redirect()->route('members.index')->with(['success' => 'Berhasil Dihapus!']);

    }
}
