<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category['category'] = Category::get();
        return view('product.category.index')->with($category);
    }

    public function data() 
    {
        $category = Category::orderBy('id', 'DESC')->get();

        return datatables()
            ->of($category)
            ->addIndexColumn()
            ->addColumn('created_at', function ($category) {
                return $category->created_at->format('d M Y');
            })
            ->addColumn('last_updated', function ($category) {
                return $category->updated_at->diffForHumans();
            })
            ->addColumn('aksi', function ($category) {
                return '
                    <button data-category="'.$category->category_name.'" data-route="'. route('categories.update', $category->id) .'" class="edit btn btn-xs btn-success btn-flat"><i class="bi bi-pencil-square"></i></button>
                    <button onclick="deleteData(`'. route('categories.destroy', $category->id) .'`)" class="btn btn-xs btn-danger btn-flat delete"><i class="bi bi-trash"></i></button>
                ';
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'category_name' => 'required|max:225|unique:category',
        ]);

        $name = $request['category_name'];

        $category = new Category;
        $category->category_name = $name;
        $category->save();

        return redirect()->route('categories.index')->with(['success' => 'Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $request->validate([
            'category_name' => 'max:225|unique:category',
        ]);

        $name = $request['category_name'];

        $category = Category::find($id);
        $category->category_name = $name;
        $category->updated_at = now();
        $category->update();

        return redirect()->route('categories.index')->with(['success' => 'Berhasil di Update!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::find($id)->delete();
        return redirect()->route('categories.index')->with(['success' => 'Berhasil di Hapus!']);
    }
}
