<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $category['category'] = Category::get();
        $category = Category::all()->pluck('id', 'category_name');
        return view('product.data.index', compact('category'));
    }

    public function checkPrice($value)
    {
        if (gettype($value) == "string") {
            $temp = 0;
            for ($i = 0; $i < strlen($value); $i++) {
                if ((isset($value[$i]) == true && $value[$i] != ".") && $value[$i] != ",") {
                    $temp = ($temp * 10) + (int)$value[$i];
                }
            }
            return $temp;
        } else {
            return $value;
        }
    }

    public function data()
    {
        $product = Product::leftJoin('category', 'category.id', 'product.category_id')
                    ->select('product.*', 'category_name')     
                    ->orderBy('id', 'desc')
                    ->get();

        return datatables()
            ->of($product)
            ->addIndexColumn()
            ->addColumn('barcode', function ($product) {
                return '<span class="badge badge-info">'. $product->barcode .'</span>';
            })
            ->addColumn('purchase', function ($product) {
                return 'Rp. '. format_uang($product->purchase);
            })
            ->addColumn('sell', function ($product) {
                return 'Rp. '. format_uang($product->sell);
            })
            ->addColumn('stock', function ($product) {
                if($product->stock == 0)
                {
                    return '<span class="badge badge-danger">Habis</span>';
                }
                else{
                    return format_uang($product->stock);
                }
            })
            ->addColumn('created_at', function ($product) {
                return $product->created_at->format('d M Y');
            })
            ->addColumn('last_updated', function ($product) {
                return $product->updated_at->diffForHumans();
            })
            ->addColumn('action', function($product) { 
                return '
                    <button data-product="'.$product->product_name.'"
                            data-category="'.$product->category_id.'"
                            data-purchase="'.$product->purchase.'"
                            data-sell="'.$product->sell.'"
                            data-stock="'.$product->stock.'"
                            data-route="'. route('products.update', $product->id) .'" 
                            class="edit btn btn-xs btn-success btn-flat"><i class="bi bi-pencil-square"></i></button>
                    <button onclick="deleteForm(`'. route('products.destroy', $product->id) .'`)" class="btn btn-xs btn-danger btn-flat"><i class="bi bi-trash"></i></button>
                    '; 
                })
            ->rawColumns(['action', 'barcode'])
            ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = Product::select('barcode')->orderBy('created_at', 'DESC')->first();
        
        $barcode = '';

        if($product == NULL) {
            $barcode = 'B-0001';
        } else {
            $barcode = sprintf('B-000%03d', substr($product->barcode, 5) + 1);
            // $kode = sprintf('BRC-202205%03d' + 1);
        }

        $sell = ($this->checkPrice($request->purchase) * 0.2) + $this->checkPrice($request->purchase);

        $request['barcode'] = $barcode;
        $request['purchase'] = $this->checkPrice($request->purchase);
        $request['sell'] = $sell;

        $product = Product::create($request->all())->save();

        return redirect()->route('products.index')->with(['success' => 'Berhasil Disimpan!']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $product->update($request->all());

        return redirect()->route('products.index')->with(['success' => 'Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::find($id)->delete();
        return redirect()->route('products.index')->with(['success' => 'Berhasil dihapus!']);
    }
}
