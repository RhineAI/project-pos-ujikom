<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Supplier;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('supplier.index');
    }

    public function data() 
    {
        $supplier = Supplier::orderBy('id', 'DESC')->get();

        return datatables()
            ->of($supplier)
            ->addIndexColumn()
            ->addColumn('company', function ($supplier) {
                return '<div style="align-items:left">'. $supplier->company .'</div>';
            })
            ->addColumn('created_at', function ($supplier) {
                return $supplier->created_at->format('d M Y');
            })
            ->addColumn('last_updated', function ($supplier) {
                return $supplier->updated_at->diffForHumans();
            })
            ->addColumn('aksi', function ($supplier) {
                return '
                    <button data-name="'.$supplier->name.'" data-adress="'.$supplier->adress.'" data-company="'.$supplier->company.'" data-route="'. route('members.update', $supplier->id) .'" class="edit btn btn-xs btn-success btn-flat"><i class="bi bi-pencil-square"></i></button>
                    <button onclick="deleteData(`'. route('members.destroy', $supplier->id) .'`)" class="btn btn-xs btn-danger btn-flat delete"><i class="bi bi-trash"></i></button>
                ';
            })
            ->rawColumns(['aksi', 'company'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $supplier = new Supplier;
        $supplier->name = $request->name;
        $supplier->adress = $request->adress;
        $supplier->company = $request->company;
        $supplier->save();

        return redirect()->route('suppliers.index')->with(['success', 'Berhasil Disimpan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supplier = Supplier::find($id);
        $supplier->name = $request->name;
        $supplier->adress = $request->adress;
        $supplier->company = $request->company;
        $supplier->update();

        return redirect()->route('suppliers.index')->with(['success', 'Berhasil Diperbarui!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Supplier::find($id)->destroy();
        return redirect()->route('suppliers.index')->with(['success', 'Berhasil Dihapus!']);
    }
}
