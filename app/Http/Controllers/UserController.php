<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.index');
    }


    public function data() 
    {
        $user = Member::orderBy('id', 'DESC')->get();

        return datatables()
            ->of($user)
            ->addIndexColumn()
            ->addColumn('level', function($user) {
                if ($user->level == 0) {
                    return '<span class="badge badge-secondary"> Kasir </span>';
                }
                elseif ($user->level == 1) {
                    return '<span class="badge badge-success"> Admin </span>';
                }
            }) 
            ->addColumn('image', function($article) {
                return '
                <img width="90%" class="rounded" src="'. asset('storage/'. $article->photo) .'">
                ';
            })
            ->addColumn('created_at', function ($user) {
                return $user->created_at->format('d M Y');
            })
            ->addColumn('last_updated', function ($user) {
                return $user->updated_at->diffForHumans();
            })
            ->addColumn('aksi', function ($user) {
                return '
                    <button data-email="'. $user->email .'"
                            data-name="'. $user->name .'" 
                            data-adress="'. $user->adress. '" 
                            data-level="' .$user->level .'" 
                            data-route="'. route('users.update', $user->id) .'" class="edit btn btn-xs btn-success btn-flat"><i class="bi bi-pencil-square"></i></button>
                    <button onclick="deleteData(`'. route('users.destroy', $user->id) .'`)" class="btn btn-xs btn-danger btn-flat delete"><i class="bi bi-trash"></i></button>
                ';
            })
            ->rawColumns(['aksi', 'grade'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'email' => 'required|max:255|email:dns',
            'name' => 'required',
            'level' => 'required',
            'image' => 'image|file|required|max:10240',
            'adress' => 'required',
            'password' => 'required'
        ]);

        if ($request->file('image')) {
            $validate['image'] = $request->file('image')->store('user-images');
        }

        $email = $request['email'];
        $name = $request['name'];
        $image = $validate['image'];
        $level = $request['level'];
        $adress = $request['adress'];
        $password = $request['password'];


        $user = new User();
        $user->email = $email;
        $user->name = $name;
        $user->level = $level;
        $user->image = $image;
        $user->adress = $adress;
        $user->password = $password;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
