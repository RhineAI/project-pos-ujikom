<!-- Modal -->
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabelLogout" aria-hidden="true">
    <div class="modal-dialog" role="document">

        <form action="" method="post" class="form-horizontal">
            @csrf
            @method('post')

            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabelLogout">Ohh No!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="email" class="col-md-2 col-md-offset-1 control-label">
                            <h5 class="my-2">Email</h5>
                        </label>
                        <div class="col-md-10">
                            <input type="email" name="email" id="email" class="form-control" required autofocus>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-2 col-md-offset-1 control-label">
                            <h5 class="my-2">Nama</h5>
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="name" id="name" class="form-control" required>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                     <div class="form-group row">
                        <label for="level" class="col-md-2 col-md-offset-1 control-label">
                            <h5 class="my-2">Peringkat</h5>
                        </label>
                        <div class="col-md-10">
                            <select name="level" id="level" class="form-control" required>
                                <option value="">Pilih</option>
                                <option value="0">Kasir</option>
                                <option value="1">Admin</option>
                            </select>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="adress" class="col-md-2 col-md-offset-1 control-label">
                            <h5 class="my-2">Alamat</h5>
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="adress" id="adress" class="form-control" required>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group col-md-11 mb-3" style="margin: auto;">
                        <label for="image" class="form-label">Foto</label>
                        <input type="file" class="form-control @error('image') is-invalid @enderror" id="image"
                            name="image" value="{{ old('image') }}" required onchange="previewImage()">
                        <img class="img-preview img-fluid my-3 col-sm-5">
                        @error('image')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-circle-check"></i>
                    Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
