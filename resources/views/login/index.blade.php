<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/assets/css/login.css">
    <title>Login | Rhyne POS</title>
</head>
<body>
    <div class="box">
        <div class="form">
            <h2>Sign In</h2>
            <form action="/login" method="post">
            @csrf
                <div class="inputBox">
                    <input type="email" id="email" name="email" name="email" required="required" autofocus
                    @error('email')
                        is-invalid    
                    @enderror> 
                    <span>Email</span>
                    <i></i>
                </div>
                <p class="help">
                    Please enter your Email
                </p>
                @error('email')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror

                <div class="inputBox">
                    <input type="password" id="password" name="password" name="password" required " 
                    @error('password')
                        is-invalid    
                    @enderror>
                    <span>Password</span>
                    <i></i>
                </div>
                <p class="help">
                    Please enter your Password
                </p>
                @error('password')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror

                <input type="submit" value="Login">
            </form>

            <div class="links">
                <a href="#"> Forgot Password </a>
                <a href="#"> Sign Up</a>
            </div>
        </div>
    </div>

    @include('templates.footer')
</body>
</html>