@extends('templates.layout')

@section('title')
    Data Member
@endsection

@section('breadcrumb')
@parent
    Member
@endsection

@push('css')
    <style>
        .luxury {
            font-family: 'Poppins', sans-serif;
        }
    </style>
@endpush

@section('content')
<div class="row mx-3">
    <div class="col-md-12 p-2 mb-3" style="background-color: white">
        <div class="box">

            <div class="box-header with-border mb-3">
                <button onclick="addForm('{{ route('members.store') }}')" class="btn btn-primary mx-2 my-3"><i
                        class="fa fa-plus-circle"></i>
                    Tambah</button>
            </div>

            <div class="box-body table-responsive">
                <!-- DataTable with Hover -->
                <div class="col-lg-12">
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Showing All Data from Database - Member</h6>
                        </div>
                        <div class="table-responsive p-3">
                            <table class="table align-items-center table-flush table-hover text-center" id="dataTableHover">
                                <thead class="thead-light">
                                    <tr>
                                        <th width="8%" class="text-center">No</th>
                                        <th width="15%" class="text-center">Name</th>
                                        <th width="15%" class="text-center">Adress</th>
                                        <th width="15%" class="text-center">Grade</th>
                                        <th width="14%" class="text-center">Created At</th>
                                        <th width="14%" class="text-center">Last Updated</th>
                                        <th width="15%" class="text-center">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@includeIf('member.form')
@endsection

@push('scripts')
    <script>
        $('#success-alert').fadeTo(1500, 500).slideUp(500, function() {
            $('#success-alert').slideUp(500);
        });
        
        $('#danger-alert').fadeTo(1500, 500).slideUp(500, function() {
            $('#danger-alert').slideUp(500);
        });


        function addForm(url) {
            $('#modal-form').modal('show')
            $('#modal-form .modal-title').text('Tambah Member Baru');
        }

        let table;
            table = $('.table').DataTable({
            processing: true,
            responsive: true,
            autoWidth: false,
            serverSide: true,
            ajax: {
                url: "{{ route('member.data') }}",
                type: "POST",
                data: {  
                    _token: '{{ csrf_token() }}'
                }
            },
            columns: [
                {data:'DT_RowIndex', searchable: false, sortable: false},
                {data:'name'},
                {data:'adress'},
                {data:'grade'},
                {data:'created_at'},
                {data:'last_updated'},
                {data:'aksi', searchable: false, sortable: false},
            ]
        });

        $(document).on('click', '.edit', function (event) {
            let name = $(this).data('name')
            let adress = $(this).data('adress')
            let level = $(this).data('level')
            let url = $(this).data('route')

            let data = {
                name : name,
                adress : adress,
                level : level,
                url: url
            }

            editForm(data)
        })
        
        function editForm(data) {
            $('#modal-form').modal('show')
            $('#modal-form .modal-title').text('Edit Member');

            $('#modal-form form')[0].reset();
            $('#modal-form form').attr('action', data.url);
            $('#modal-form [name=_method]').val('put');
            
            $('#modal-form [name=name]').val(data.name);
            $('#modal-form [name=adress]').val(data.adress);
            $('#modal-form [name=level]').val(data.level);
        }

        function deleteData(url) {
            Swal.fire({
                title: 'Are you sure?',
                text: "Data akan dihapus",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                }).then((result) => {
                if (result.isConfirmed) {
                    $.post(url, {
                        '_token': $('[name=token]').attr('content'),
                        '_method': 'delete'
                    })
                    .done((response) => {
                        Swal.fire({
                            title: 'Sukses!',
                            text: 'Data berhasil dihapus',
                            icon: 'success',
                            confirmButtonText: 'Lanjut',
                            confirmButtonColor: '#28A745'
                        }) 
                        table.ajax.reload();
                    })
                    .fail((errors) => {
                        Swal.fire({
                            title: 'Gagal!',
                            text: 'Data gagal dihapus',
                            icon: 'error',
                            confirmButtonText: 'Kembali',
                            confirmButtonColor: '#DC3545'
                        })                       
                        return;
                    });
                } else if (result.isDenied) {
                    Swal.fire({
                        title: 'Data batal dihapus',
                        icon: 'warning',
                    })
                }
            })
        }

    </script>    
@endpush