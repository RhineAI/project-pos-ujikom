<!-- Modal -->
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabelLogout" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">

        <form action="" method="post" class="form-horizontal">
            @csrf
            @method('post')

            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabelLogout">Ohh No!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="product_name" class="col-md-2 col-md-offset-1 control-label">
                            <h5 class="my-2">Nama Produk</h5>
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="product_name" id="product_name" class="form-control" required autofocus>
                            <p class="help">The name of Product must be Unique!</p>
                            <div id="product_name" class="invalid-feedback">
                                Must be a Unique.
                            </div>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="category_id" class="col-md-2 col-md-offset-1 control-label">
                            <h5 class="my-2">Kategori</h5>
                        </label>
                        <div class="col-md-10">
                            <select name="category_id" id="category_id" class="form-control" required>
                                <option value="">Pilih Kategori</option>
                                @foreach ($category as $key => $item )
                                    <option value="{{ $item }}">{{ $key }}</option>
                                @endforeach
                            </select>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="input-group mb-3 mx-1"> 
                        <label for="purchase" style="margin-left: -16px; margin-right:17.6px;" class="col-md-2 control-label">
                            <h5 class="my-2">Harga Beli</h5> 
                        </label>
                        <div class="input-group-prepend"> 
                            <span class="input-group-text">RP.</span> 
                        </div> 
                        <input type="text" name="purchase" id="purchase" class="form-control" required placeholder="0" maxlength="20" aria-describedby="basic-addon1">
                        <span class="help-block with-errors"></span>
                    </div>

                    <div class="input-group mb-3 mx-1"> 
                        <label for="sell" style="margin-left: -16px; margin-right:17.6px;" class="col-md-2 control-label">
                            <h5 class="my-2">Harga Jual</h5> 
                        </label>
                        <div class="input-group-prepend "> 
                            <span class="input-group-text">RP.</span> 
                        </div> 
                        <input readonly type="text" name="sell" id="sell" class="form-control" required placeholder="0" maxlength="20" aria-describedby="basic-addon1">
                        <span class="help-block with-errors"></span>
                    </div>

                    <div class="form-group row">
                        <label for="stock" class="col-md-2 col-md-offset-1 control-label">
                            <h5 class="my-2">Stok</h5>
                        </label>
                        <div class="col-md-10">
                            <input type="number" name="stock" id="stock" class="form-control" required autofocus>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-circle-check"></i>
                    Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
