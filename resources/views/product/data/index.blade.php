@extends('templates.layout')

@section('title')
    Data Produk
@endsection

@section('breadcrumb')
@parent
    Produk
@endsection

@section('content')
<div class="row mx-3">
    <div class="col-md-12 p-2 mb-3" style="background-color: white">
        <div class="box">

            <div class="box-header with-border mb-3">
                <button onclick="addForm('{{ route('products.store') }}')" class="btn btn-primary mx-2 my-3"><i
                        class="fa fa-plus-circle"></i>
                    Tambah</button>
            </div>

            <div class="box-body table-responsive">
                <!-- DataTable with Hover -->
                <div class="col-lg-12">
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Showing All Data from Database - Product</h6>
                        </div>
                        <div class="table-responsive p-3">
                            <table class="table align-items-center table-flush table-hover text-center" id="dataTableHover">
                                <thead class="thead-light">
                                    <tr>
                                        <th width="6%" class="text-center">No</th>
                                        <th width="6%" class="text-center">Barcode</th>
                                        <th width="15%" class="text-center">Name</th>
                                        <th width="15%" class="text-center">Category</th>
                                        <th width="15%" class="text-center">Purchase</th>
                                        <th width="15%" class="text-center">Sell</th>
                                        <th width="8%" class="text-center">Stock</th>
                                        <th width="14%" class="text-center">Created At</th>
                                        <th width="14%" class="text-center">Last Updated</th>
                                        <th width="20%" class="text-center">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@includeIf('product.data.form')
@endsection

@push('scripts')
    <script>
        $('#success-alert').fadeTo(1500, 500).slideUp(500, function() {
            $('#success-alert').slideUp(500);
        });
        
        $('#danger-alert').fadeTo(1500, 500).slideUp(500, function() {
            $('#danger-alert').slideUp(500);
        });


        function formatRupiah(angka, prefix){
            var number_string   = angka.replace(/[^,\d]/g, '').toString(),
            split               = number_string.split(','),
            sisa                = split[0].length % 3,
            rupiah              = split[0].substr(0, sisa),
            ribuan              = split[0].substr(sisa).match(/\d{3}/gi);

            if(ribuan){
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
        }

        function generateRupiah(elemValue) {
            return $(elemValue).val(formatRupiah($(elemValue).val(), 'Rp. '))
        }

        $(document).on('keyup', '#purchase', function(e){
            generateRupiah(this);
        })
        $(document).on('keyup', '#sell', function(e){
            generateRupiah(this);
        })


        function addForm(url) {
            $('#modal-form').modal('show')
            $('#modal-form .modal-title').text('Tambah Produk Baru');
        }

        let table;
            table = $('.table').DataTable({
            processing: true,
            responsive: true,
            autoWidth: false,
            serverSide: true,
            ajax: {
                url: "{{ route('product.data') }}",
                type: "POST",
                data: {  
                    _token: '{{ csrf_token() }}'
                }
            },
            columns: [
                {data:'DT_RowIndex', searchable: false, sortable: false},
                {data:'barcode'},
                {data:'product_name'},
                {data:'category_name'},
                {data:'purchase'},
                {data:'sell'},
                {data:'stock'},
                {data:'created_at'},
                {data:'last_updated'},
                {data:'action', searchable: false, sortable: false},
            ]
        });
        
        $(document).on('click', '.edit', function (event) {
            let product_name = $(this).data('product')
            let category_id = $(this).data('category_id')
            let purchase = $(this).data('purchase')
            let sell = $(this).data('sell')
            let stock = $(this).data('stock')
            let url = $(this).data('route')

            let data = {
                product_name : product_name,
                category_id : category_id,
                purchase : purchase,
                sell : sell,
                stock : stock,
                url: url
            }

            editForm(data)
        })
        
        function editForm(data) {
            $('#modal-form').modal('show')
            $('#modal-form .modal-title').text('Edit Produk');

            $('#modal-form form')[0].reset();
            $('#modal-form form').attr('action', data.url);
            $('#modal-form [name=_method]').val('put');
            
            $('#modal-form [name=product_name]').val(data.product_name);
            $('#modal-form [name=category_id]').val(data.category_id);
            $('#modal-form [name=purchase]').val(data.purchase);
            $('#modal-form [name=sell]').val(data.sell);
            $('#modal-form [name=stock]').val(data.stock);
        }

        function deleteForm(url) {
            Swal.fire({
                title: 'Are you sure?',
                text: "Data akan dihapus",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                }).then((result) => {
                if (result.isConfirmed) {
                    $.post(url, {
                        '_token': $('[name=token]').attr('content'),
                        '_method': 'delete'
                    })
                    .done((response) => {
                        Swal.fire({
                            title: 'Sukses!',
                            text: 'Data berhasil dihapus',
                            icon: 'success',
                            confirmButtonText: 'Lanjut',
                            confirmButtonColor: '#28A745'
                        }) 
                        table.ajax.reload();
                    })
                    .fail((errors) => {
                        Swal.fire({
                            title: 'Gagal!',
                            text: 'Data gagal dihapus',
                            icon: 'error',
                            confirmButtonText: 'Kembali',
                            confirmButtonColor: '#DC3545'
                        })                       
                        return;
                    });
                } else if (result.isDenied) {
                    Swal.fire({
                        title: 'Data batal dihapus',
                        icon: 'warning',
                    })
                }
            })
        }
    
    </script>    
@endpush