<!-- Modal -->
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabelLogout" aria-hidden="true">
    <div class="modal-dialog" role="document">

        <form action="" method="post" class="form-horizontal">
            @csrf
            @method('post')

            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabelLogout">Ohh No!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="category_name" class="col-md-2 col-md-offset-1 control-label">
                            <h5 class="my-2">Kategori</h5>
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="category_name" id="category_name" class="form-control" required autofocus>
                            <div id="category_name" class="invalid-feedback">
                                Must be a Unique.
                            </div>
                            <span class="help-block with-errors"></span>
                        </div>
                        <p class="help">The name of Unit must be Unique!</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-circle-check"></i>
                    Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
