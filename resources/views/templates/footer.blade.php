
<script src="{{ asset('assets') }}/vendor/jquery/jquery.min.js"></script>
{{-- <script src="{{ asset('assets') }}/vendor/datatables/jquery.dataTables.min.js"></script> --}}
{{-- <script src="{{ asset('assets') }}/vendor/datatables/dataTables.bootstrap4.min.js"></script> --}}
<script src="{{ asset('assets') }}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="{{ asset('assets') }}/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="{{ asset('assets') }}/js/ruang-admin.min.js"></script>
<script src="{{ asset('assets') }}/vendor/chart.js/Chart.min.js"></script>
<script src="{{ asset('assets') }}/js/demo/chart-area-demo.js"></script> 

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>

{{-- Bootstrap 5 --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.9.1/font/bootstrap-icons.min.css"><script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.0.1/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js
"></script>

{{-- SweetAlert2 --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.29/sweetalert2.min.js"></script>
<link rel="stylesheet" href="sweetalert2.min.css">
<script src="sweetalert2.all.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

{{-- Toastr --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

{{-- DataTables --}}
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>

<script>
    @if(session()->has('success'))
        toastr.success('{{ session('success') }}', 'TERIMA KASIH!'); 

        @elseif(session()->has('error'))

        toastr.error('{{ session('error') }}', 'GAGAL!'); 
    @endif
</script>


@stack('scripts')